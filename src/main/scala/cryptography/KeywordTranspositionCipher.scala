package org.sonofizanagi.algo.cryptography

/**
 * A keyword transposition cipher is a method of choosing a monoalphabetic substitution
 * with which to encode a message. The substitution alphabet is determined by choosing
 * a keyword, arranging the remaining letters of the alphabet in columns below the
 * letters of the keyword, and then reading back the columns in the alphabetical order
 * of the letters of the keyword.
 *
 * For instance, if one chose the keyword SECRET, the columns generated would look like
 * the following diagram. Note how the letters in the keyword are skipped when laying out
 * the columns, and duplicate letters are removed from the keyword:
 *
 * SECRT
 * ABDFG
 * HIJKL
 * MNOPQ
 * UVWXY
 * Z
 *
 * Since the alphabetical order of the characters in the keyword is CERST, the columns are
 * then read back in that order to get the substitution alphabet.
 *
 * Original:     ABCDE FGHIJ KLMNO PQRSTU VWXYZ
 * Substitution: CDJOW EBINV RFKPX SAHMUZ TGLQY
 *
 * Given a piece of ciphertext and the keyword used to encipher it with the keyword transposition
 * cipher described above, write an algorithm to output the original message.
 *
 * @see https://www.hackerrank.com/challenges/keyword-transposition-cipher
 *
 */
object KeywordTranspositionCipher {

  def decrypt(word: String, ciphertext: List[String]): List[String] = {
    val keyword = word.distinct
    val cipher = buildCipher(keyword)
    val inverseMap = cipher.zip('A' to 'Z').toMap
    ciphertext map { ss =>
      ss.map(s => inverseMap.getOrElse(s, ' '))
    }
  }

  def buildCipher(keyword: String): String = {
    val alphabet = keyword ++ ('A' to 'Z' diff keyword)
    val rows = alphabet.grouped(keyword.size).toList
    val cipher = transpose(rows)
    cipher.sorted.mkString
  }

  /**
   * Scala's transpose function works on lists of equal size.
   */
  def transpose(rows: List[String]): List[String] = {
    val res = rows.head.size match {
      case _ if (rows.head.size == rows.last.size) => rows.transpose
      case _ =>
        {
          val tmp = rows.init.transpose
          val begining = (tmp zip rows.last) map { t => t._1 :+ t._2 }
          begining ++ tmp.drop(begining.size)
        }
    }
    res.map(_.mkString)
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(2).foreach(line => printResult(line))
  }

  def printResult(lines: List[String]): Unit = {
    val keyword = lines.head
    val ciphertext = lines.tail.head.split(" ").toList
    val result = decrypt(keyword, ciphertext)
    println(result.mkString(" "))
  }
}