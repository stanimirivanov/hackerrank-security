package org.sonofizanagi.algo.cryptography

/**
 * Given a piece of text encoded with a simple monoalphabetic substitution cipher, use
 * basic cryptanalytic techniques to attempt to recover the original plaintext. You
 * will be provided with a special dictionary file that you can read from dictionary.lst,
 * which will consist of one word per line.
 *
 * The input ciphertext will consist of a string of space separated enciphered words
 * from the provided dictionary.lst file that is accessible to your code. The dictionary
 * contains technical jargon and may not be entirely representative of English-language
 * plaintext, so frequency analysis techniques may not be entirely helpful.
 *
 * You will output the decrypted plaintext. The output will be a string of space separated
 * words from the provided dictionary. You'll be scored based on how accurately you recover
 * the original text.
 *
 * @see https://www.hackerrank.com/challenges/basic-cryptanalysis
 */
object BasicCryptanalysis {

  def decrypt(text: List[String]): List[String] = {
    val PATH = getClass.getResource("").getPath
    val source = scala.io.Source.fromFile(PATH + "dictionary.lst")
    val lines = source.getLines.toList
    source.close()
    val dictionary = lines.groupBy(s => s.size)

    def buildKey(text: List[String], key: Map[Char, Char]): Map[Char, Char] = {
      if (text.isEmpty) key
      else {
        val encripted = text.head
        val additionalKey = dictionary.getOrElse(encripted.size, Nil) match {
          case List(candidate) => mapKey(candidate, encripted)
          case candidates => candidates.foldLeft(Map[Char, Char]()) { (acc, candidate) =>
            if (matchingWords(candidate, encripted)) acc ++ mapKey(candidate, encripted) else acc
          }
        }

        val verifiedKey = additionalKey.filter {
          case (k, v) => key.get(k) == None || key.get(k) == Some(v)
        }
        val merged = if (verifiedKey == additionalKey) key ++ additionalKey else key
        buildKey(text.tail, merged)
      }
    }

    val key = buildKey(text.sortWith(_.size > _.size), Map.empty)
    val attempt = text.map(word => word.map(c => key.getOrElse(c, '.')).mkString)
    attempt map { word =>
      word match {
        case _ if word.contains('.') => {
          dictionary.getOrElse(word.size, Nil).foldLeft(word) { (acc, dictWord) =>
            val pattern = word.r
            pattern findFirstIn dictWord match {
              case Some(v) => dictWord
              case None    => acc
            }
          }
        }
        case _ => word
      }
    }
  }

  def mapKey(w1: String, w2: String): Map[Char, Char] =
    (w2 zip w1).toMap

  def matchingWords(w1: String, w2: String): Boolean = {
    val pairs1 = equalIndexPairs(w1)
    val pairs2 = equalIndexPairs(w2)
    pairs1 == pairs2 && !pairs1.filter(_.size > 1).isEmpty
  }

  def equalIndexPairs(word: String): Seq[Seq[Int]] =
    word.map(s => word.zipWithIndex.filter(_._1 == s).map(_._2))

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(line: String): Unit = {
    val text = line.split(" ").toList
    val result = decrypt(text)
    println(result.mkString(" "))
  }
}