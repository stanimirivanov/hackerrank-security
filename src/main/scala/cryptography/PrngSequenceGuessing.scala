package org.sonofizanagi.algo.cryptography

/**
 * Given a time range during which the Java Random generator was seeded and 
 * the last ten output values of random.nextInt(1000), guess the next value 
 * to be output by the generator.
 * 
 * @see https://www.hackerrank.com/challenges/prng-sequence-guessing
 *
 */
object PrngSequenceGuessing {
  
  import scala.util.Random

  def guessSeed(startTime: Int, endTime: Int, nums: List[Int]): List[Int] = {
    def loop(seed: Int): (Int, Random) = {
      if (seed > endTime) (-1, new Random(-1))
      else {
        val rand = new Random(seed)
        val found = nums.foldLeft(true)((acc, n) => acc && rand.nextInt(1000) == n)
        if (found) (seed, rand)
        else loop(seed + 1)
      }
    }

    val result = loop(startTime)
    val seed = result._1
    val rand = result._2
    seed :: (1 to 10).map(n => rand.nextInt(1000)).toList
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.grouped(11).foreach(ls => printResult(ls))
  }

  def printResult(lines: List[String]): Unit = {
    val timestamps = lines.head.split(" ").map(_.toInt).toArray
    val startTime = timestamps(0)
    val endTime = timestamps(1)
    val nums = lines.tail.map(_.toInt)
    val result = guessSeed(startTime, endTime, nums)
    println(result.mkString(" "))
  }
}