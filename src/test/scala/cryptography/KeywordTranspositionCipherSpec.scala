package org.sonofizanagi.algo.cryptography

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class KeywordTranspositionCipherSpec extends FlatSpec {

  "Keyword Transposition Cipher" should "pass test cases" in {

    val keywords = Array("SPORT", "SECRET")
    val ciphertextArray = Array(List("LDXTW", "KXDTL", "NBSFX", "BFOII", "LNBHG", "ODDWN", "BWK"),
      List("JHQSU", "XFXBQ"))

    val expected = Array(List("ILOVE", "SOLVI", "NGPRO", "GRAMM", "INGCH", "ALLEN", "GES"),
      List("CRYPT", "OLOGY"))

    0 to 1 foreach { i =>
      val keyword = keywords(i)
      val ciphertext = ciphertextArray(i)
      val result = KeywordTranspositionCipher.decrypt(keyword, ciphertext)
      assert(result === expected(i))
    }
  }

}