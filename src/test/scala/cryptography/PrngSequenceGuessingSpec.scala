package org.sonofizanagi.algo.cryptography

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PrngSequenceGuessingSpec extends FlatSpec {

  "Prng Sequence Guessing" should "pass test cases" in {

    val startTimes = Array(1374037200, 1374037299)
    val endTimes = Array(1374123600, 1374143600)
    val numsArray = Array(List(643, 953, 522, 277, 464, 366, 321, 409, 227, 702),
      List(877, 654, 2, 715, 229, 255, 712, 267, 19, 832))

    val expected = Array(List(1374037200, 877, 633, 491, 596, 839, 875, 923, 461, 27, 826),
      List(1374037459, 101, 966, 573, 339, 784, 718, 949, 934, 62, 368))

    0 to 1 foreach { i =>
      val startTime = startTimes(i)
      val endTime = endTimes(i)
      val nums = numsArray(i)
      val result = PrngSequenceGuessing.guessSeed(startTime, endTime, nums)
      assert(result === expected(i))
    }
  }

}